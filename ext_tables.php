<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Georgy.WkhtmlPdf',
            'Wkhtmlpdf',
            'wkhtmlpdf'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('wkhtml_pdf', 'Configuration/TypoScript', 'wkhtmlpdf');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wkhtmlpdf_domain_model_wkhtmlpdf', 'EXT:wkhtml_pdf/Resources/Private/Language/locallang_csh_tx_wkhtmlpdf_domain_model_wkhtmlpdf.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wkhtmlpdf_domain_model_wkhtmlpdf');

    }
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_wkhtmlpdf';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature]='select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/Flexform/flexform.xml');
