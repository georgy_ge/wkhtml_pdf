<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Georgy.WkhtmlPdf',
            'Wkhtmlpdf',
            [
                'Wkhtmlpdf' => 'list,downloadPage'
            ],
            // non-cacheable actions
            [
                'Wkhtmlpdf' => 'list,downloadPage'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    wkhtmlpdf {
                        iconIdentifier = wkhtml_pdf-plugin-wkhtmlpdf
                        title = LLL:EXT:wkhtml_pdf/Resources/Private/Language/locallang_db.xlf:tx_wkhtml_pdf_wkhtmlpdf.name
                        description = LLL:EXT:wkhtml_pdf/Resources/Private/Language/locallang_db.xlf:tx_wkhtml_pdf_wkhtmlpdf.description
                        tt_content_defValues {
                            CType = list
                            list_type = wkhtmlpdf_wkhtmlpdf
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'wkhtml_pdf-plugin-wkhtmlpdf',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:wkhtml_pdf/Resources/Public/Icons/user_plugin_wkhtmlpdf.svg']
			);
		
    }
);
