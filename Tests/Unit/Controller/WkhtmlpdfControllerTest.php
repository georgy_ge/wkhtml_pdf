<?php
namespace Georgy\WkhtmlPdf\Tests\Unit\Controller;

/**
 * Test case.
 */
class WkhtmlpdfControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Georgy\WkhtmlPdf\Controller\WkhtmlpdfController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Georgy\WkhtmlPdf\Controller\WkhtmlpdfController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllWkhtmlpdfsFromRepositoryAndAssignsThemToView()
    {

        $allWkhtmlpdfs = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $wkhtmlpdfRepository = $this->getMockBuilder(\Georgy\WkhtmlPdf\Domain\Repository\WkhtmlpdfRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $wkhtmlpdfRepository->expects(self::once())->method('findAll')->will(self::returnValue($allWkhtmlpdfs));
        $this->inject($this->subject, 'wkhtmlpdfRepository', $wkhtmlpdfRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('wkhtmlpdfs', $allWkhtmlpdfs);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
