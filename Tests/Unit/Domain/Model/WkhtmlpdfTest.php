<?php
namespace Georgy\WkhtmlPdf\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class WkhtmlpdfTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Georgy\WkhtmlPdf\Domain\Model\Wkhtmlpdf
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Georgy\WkhtmlPdf\Domain\Model\Wkhtmlpdf();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
