<?php
namespace Georgy\WkhtmlPdf\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use Georgy\WkhtmlPdf\Utility\MailsendUtility;

/***
 *
 * This file is part of the "wkhtmlpdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * WkhtmlpdfController
 */
class WkhtmlpdfController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * wkhtmlpdfRepository
     * 
     * @var \Georgy\WkhtmlPdf\Domain\Repository\WkhtmlpdfRepository
     * @inject
     */
    protected $wkhtmlpdfRepository = null;

    /**
     * __construct
     *
     *  constructor.
     */
    public function __construct()
    {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->pdfGenerator = $objectManager->get('mikehaertl\wkhtmlto\Pdf',array('ignoreWarnings' => true));
        
    }
    
    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $data = $this->request->getArguments('download');
        $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL');
        if($data['download'] == 'download' && $this->settings['sendEmail']) {
            //Fetching binary path value from extension manager configuration
            $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['wkhtml_pdf']);
            $this->pdfGenerator->binary = $extensionConfiguration['binaryPath'];
            $this->pdfGenerator->setOptions(array(
                'header-right' => !empty($this->getHeaderText()) ? $this->getHeaderText() : '',
                'header-font-size' => !empty($this->getHeaderSize()) ? $this->getHeaderSize() : '5',
                'footer-left' => $this->settings['footerPagination'] ? "Page [page] of [toPage]" : '',
                'footer-font-size' => $this->settings['footerPagination'] ? $this->settings['footerSize'] : '5',
            ));
            //$pdfGenerator->binary = 'typo3conf/ext/wkhtml_pdf/Libraries/bin/wkhtmltopdf';
            $directorypath = PATH_site;
            $url = $data['url'];
            $this->pdfGenerator->addPage($url);
            $this->pdfSaveMode($directorypath);
            $senderName = $this->settings['senderName'];
            $senderEmail = $this->settings['senderEmail'];
            $receiverName = $data['receiverName'];
            $receiverEmail = $data['receiverEmail'];
            $mailSent = MailsendUtility::sendPlainMail($senderName,$senderEmail,$receiverName,$receiverEmail);
        }
            //$pdfGenerator->binary = 'c:/program files/wkhtmltopdf/bin/wkhtmltopdf.exe';
        elseif ($data['download'] == 'download') {
            //Fetching binary path value from extension manager configuration
            $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['wkhtml_pdf']);
            $this->pdfGenerator->binary = $extensionConfiguration['binaryPath'];
            $this->pdfGenerator->setOptions(array(
                'header-right' => !empty($this->getHeaderText()) ? $this->getHeaderText() : '',
                'header-font-size' => !empty($this->getHeaderSize()) ? $this->getHeaderSize() : '5',
                'footer-left' => $this->settings['footerPagination'] ? "Page [page] of [toPage]" : '',
                'footer-font-size' => $this->settings['footerPagination'] ? $this->settings['footerSize'] : '5',
            ));
            //$pdfGenerator->binary = 'typo3conf/ext/wkhtml_pdf/Libraries/bin/wkhtmltopdf';
            $directorypath = PATH_site;
            $url = $data['url'];
            $this->pdfGenerator->addPage($url);
            switch ($this->settings['modeOfDownload']) {
                case "pdf_Download":
                    $this->pdfDownloadMode($directorypath);
                    break;
                case "pdf_Save":
                    $saveMode = $this->pdfSaveMode($directorypath);
                    break;
                case "pdf_Inline":
                    $this->pdfInlineMode($directorypath);
                    break;
                default :
                    $this->pdfInlineMode($directorypath);
            }
        }
        $this->view->assignMultiple(
            [
                'url'       => $url,
                'settings'  => $this->settings,
                'mailSent'  => $mailSent,
            ]
        );
    }
    
    /**
     * getHeaderOptions
     *
     * @return void
     */
    public function getHeaderText() {
        return $this->settings['headerText'];
    }
    
    /**
     * getHeaderSize
     * 
     * @return void
     */
    public function getHeaderSize() {
        return $this->settings['headerSize'];
    }
    
    /**
     * pdfDownloadMode
     * 
     * @return void
     */
    public function pdfDownloadMode($directorypath) {
        if (!$this->pdfGenerator->saveAs($directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf')) {
            throw new \Exception('Could not create PDF: '.$pdfGenerator->getError());
        } 
        else {
            //Force downloading the pdf
            $path = $directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf';
            header('Pragma: public');   // required
            header('Expires: 0');       // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.'application/pdf');
            header('Content-Disposition: attachment; filename="'.basename($path).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($path));    // provide file size
            header('Connection: close');
            readfile($path);     // push it out
            exit();
        }
    }
    
    /**
     * pdfSaveMode
     * 
     * @return void
     */
    public function pdfSaveMode($directorypath) {
        if (!$this->pdfGenerator->saveAs($directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf')) {
            throw new \Exception('Could not create PDF: '.$this->pdfGenerator->getError());
        } 
        else {
            $this->pdfGenerator->saveAs($directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf');
            return true;
        }
    }
    
    /**
     * pdfInlineMode
     * 
     * @return void
     */
    public function pdfInlineMode($directorypath) {
        if (!$this->pdfGenerator->saveAs($directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf')) {
            throw new \Exception('Could not create PDF: '.$this->pdfGenerator->getError());
        } 
        else {
            $path = $directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf';
            ob_start();
            header('Content-Type: application/pdf');
            header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
            header('Pragma: public');
            header('Expires: 0'); 
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
            header('Content-Length: '.filesize($path));    // provide file size
            header('Content-Disposition: inline; filename="'.basename($path).'"');
            ob_clean();   
            ob_end_flush();
            readfile($path);
            exit();
        }
    }
}
