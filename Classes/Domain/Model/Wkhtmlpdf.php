<?php
namespace Georgy\WkhtmlPdf\Domain\Model;

/***
 *
 * This file is part of the "wkhtmlpdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Wkhtmlpdf
 */
class Wkhtmlpdf extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
