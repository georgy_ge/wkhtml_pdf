<?php
namespace Georgy\WkhtmlPdf\Domain\Repository;

/***
 *
 * This file is part of the "wkhtmlpdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for Wkhtmlpdfs
 */
class WkhtmlpdfRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
