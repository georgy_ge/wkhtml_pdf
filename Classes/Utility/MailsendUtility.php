<?php

namespace Georgy\WkhtmlPdf\Utility;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "wkhtmltopdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 georgy <georgy.ge@pitsolutions.com>, PIT solutions
 *
 ***/

/**
 * Class MailsendUtility
 *
 * @package Georgy\WkhtmlPdf\Utility
 */
class MailsendUtility
{

    /**
     * Send a plain mail for simple notifies
     *
     * @param string $receiverEmail Email address to send to
     * @param string $senderEmail Email address from sender
     * @param string $subject Subject line
     * @param string $body Message content
     * @return bool mail was sent?
     */
    public static function sendPlainMail($senderName,$senderEmail,$receiverName,$receiverEmail)
    {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\Extbase\\Object\\ObjectManager');
        $localizationUtility = $objectManager->get('TYPO3\\CMS\\Extbase\\Utility\\LocalizationUtility');
        $directorypath = PATH_site;
        $mailSubject = $localizationUtility->translate('mailsubject', 'WkhtmlPdf');
        $mailBody = $localizationUtility->translate('mailbody', 'WkhtmlPdf');
        $senderCleanName  = filter_var($senderName,FILTER_SANITIZE_STRING);
        $senderCleanEmail = filter_var($senderEmail,FILTER_SANITIZE_EMAIL);
        $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $mail->setSubject($mailSubject);
        $mail->setFrom(array($senderCleanEmail => $senderCleanName));
        $mail->setTo(array($receiverEmail => $receiverName));
        $mail->setBody($mailBody, 'text/html', 'utf-8');
        $mail->attach(\Swift_Attachment::fromPath($directorypath . 'typo3conf/ext/wkhtml_pdf/Pdfss/report.pdf'));
        $mail->send();
        return $mail->isSent();
    }
}