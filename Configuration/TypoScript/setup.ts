
plugin.tx_wkhtmlpdf_wkhtmlpdf {
    view {
        templateRootPaths.0 = EXT:wkhtml_pdf/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_wkhtmlpdf_wkhtmlpdf.view.templateRootPath}
        partialRootPaths.0 = EXT:wkhtml_pdf/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_wkhtmlpdf_wkhtmlpdf.view.partialRootPath}
        layoutRootPaths.0 = EXT:wkhtml_pdf/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_wkhtmlpdf_wkhtmlpdf.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_wkhtmlpdf_wkhtmlpdf.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}
page.includeCSS {
    wkhtmltopdf = EXT:wkhtml_pdf/Resources/Public/Css/Basic.css
}
page.includeJSFooter {
    wkhtmltopdf = EXT:wkhtml_pdf/Resources/Public/Js/Basic.js
}

# these classes are only used in auto-generated templates
plugin.tx_wkhtmlpdf._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-wkhtml-pdf table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-wkhtml-pdf table th {
        font-weight:bold;
    }

    .tx-wkhtml-pdf table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)
