
plugin.tx_wkhtmlpdf_wkhtmlpdf {
    view {
        # cat=plugin.tx_wkhtmlpdf_wkhtmlpdf/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:wkhtml_pdf/Resources/Private/Templates/
        # cat=plugin.tx_wkhtmlpdf_wkhtmlpdf/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:wkhtml_pdf/Resources/Private/Partials/
        # cat=plugin.tx_wkhtmlpdf_wkhtmlpdf/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:wkhtml_pdf/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_wkhtmlpdf_wkhtmlpdf//a; type=string; label=Default storage PID
        storagePid =
    }
}
